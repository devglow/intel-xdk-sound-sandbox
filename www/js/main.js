var audioBackgroundMusic;
var audioSoundPop;

var currentSountLevel;

function preLoadSounds()
{
    intel.xdk.player.loadSound('music/backgroundmusic1.wav',1);
    intel.xdk.player.loadSound('sounds/pop.wav',10);
}

function playBackgroundMusic()
{
    if (intel.xdk.isnative)
        intel.xdk.player.startAudio('/music/backgroundmusic1.wav', true);
    else
        audioBackgroundMusic.play();    
}

function stopBackgroundMusic()
{
    if (intel.xdk.isnative)
        intel.xdk.player.stopAudio();
    else
        audioBackgroundMusic.pause(); 
}

function playSoundPop()
{
    if (intel.xdk.isnative)
        intel.xdk.player.playSound('/sounds/pop.wav');
    else
        audioSoundPop.play();    
}

function setSoundLevel(upDown)
{
    var level;
    var currentLevel;
    
    if (intel.xdk.isnative)
        currentLevel = currentSountLevel;
    else
        currentLevel = currentSountLevel;    
    
    if (upDown == 'up')
    {
        switch(currentLevel)
        {
            case 0:
                level = 0.33;
                break;
            case 0.33:
                level = 0.66;
                break;
            case 0.66:
                level = 1
                break;
            case 1:
                level = 1;
                break;

        }

    }
    else
    {
        switch(currentLevel)
        {
            case 0:
                level = 0;
                break;
            case 0.33:
                level = 0;
                break;
            case 0.66:
                level = 0.33
                break;
            case 1:
                level = 0.66;
                break;

        }
    }
    
    if (intel.xdk.isnative)
    {
        intel.xdk.player.setAudioVolume(level);
    }
    else
    {
        audioBackgroundMusic.volume = level;
        audioSoundPop.volume = level;
    }    
    currentSountLevel = level;
}


function mainStartup()
{
    if (intel.xdk.isnative)
    {
        preLoadSounds();
    }
    else  //  if (intel.xdk.isweb)
    {
        audioBackgroundMusic = document.getElementById("audioBackgroundMusic");
        audioSoundPop = document.getElementById("audioSoundPop");
    }
    currentSountLevel = 0.33;
    setSoundLevel('up');
    
}
